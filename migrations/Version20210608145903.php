<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210608145903 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE listing CHANGE price_total price_total INT NOT NULL');
        $this->addSql('ALTER TABLE product CHANGE id_listing id_listing INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user ADD username VARCHAR(25) NOT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649F85E0677 ON user (username)');
        $this->addSql('ALTER TABLE list_user DROP date_assignment');
        $this->addSql('ALTER TABLE list_user RENAME INDEX id_user TO IDX_C85D09A06B3CA4B');
        $this->addSql('ALTER TABLE list_user RENAME INDEX id_listing TO IDX_C85D09A0159DE1A4');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE list_user ADD date_assignment DATE NOT NULL');
        $this->addSql('ALTER TABLE list_user RENAME INDEX idx_c85d09a0159de1a4 TO id_listing');
        $this->addSql('ALTER TABLE list_user RENAME INDEX idx_c85d09a06b3ca4b TO id_user');
        $this->addSql('ALTER TABLE listing CHANGE price_total price_total INT DEFAULT 0 NOT NULL');
        $this->addSql('ALTER TABLE product CHANGE id_listing id_listing INT NOT NULL');
        $this->addSql('DROP INDEX UNIQ_8D93D649F85E0677 ON user');
        $this->addSql('ALTER TABLE user DROP username');
    }
}
