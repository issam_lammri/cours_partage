<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ListUser
 *
 * @ORM\Table(name="list_user", indexes={@ORM\Index(name="id_user", columns={"id_user"}), @ORM\Index(name="id_listing", columns={"id_listing"})})
 * @ORM\Entity
 */
class ListUser
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_user_list", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idUserList;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_ass", type="date", nullable=false)
     */
    private $dateAss;

    /**
     * @var \Listing
     *
     * @ORM\ManyToOne(targetEntity="Listing")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_listing", referencedColumnName="id_listing")
     * })
     */
    private $idListing;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_user", referencedColumnName="id_user")
     * })
     */
    private $idUser;

    public function getIdUserList(): ?int
    {
        return $this->idUserList;
    }

    public function getDateAss(): ?\DateTimeInterface
    {
        return $this->dateAss;
    }

    public function setDateAss(\DateTimeInterface $dateAss): self
    {
        $this->dateAss = $dateAss;

        return $this;
    }

    public function getIdListing(): ?Listing
    {
        return $this->idListing;
    }

    public function setIdListing(?Listing $idListing): self
    {
        $this->idListing = $idListing;

        return $this;
    }

    public function getIdUser(): ?User
    {
        return $this->idUser;
    }

    public function setIdUser(?User $idUser): self
    {
        $this->idUser = $idUser;

        return $this;
    }


}
