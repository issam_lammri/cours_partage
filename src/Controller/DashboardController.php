<?php

namespace App\Controller;

use App\Entity\Listing;
use App\Entity\ListUser;
use App\Entity\Product;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractController
{
    /**
     * @Route("/", name="dashboard")
     */
    public function index(): Response
    {
        $listings = $this->getDoctrine()
            ->getRepository(Listing::class)
            ->findAll();

        $products = $this->getDoctrine()
            ->getRepository(Product::class)
            ->findByIdListing(2);

        $listUsers = $this->getDoctrine()
            ->getRepository(ListUser::class)
            ->findByIdUser(2);
        $tableProduit=  [];
        foreach ($listUsers as $a){
            //dump($a->getIdListing()->getIdListing());
            $products = $this->getDoctrine()
                ->getRepository(Product::class)
                ->findByIdListing($a->getIdListing()->getIdListing());
            $tableProduit[$a->getIdListing()->getIdListing()]=$products;
            //dump($a->getIdListing()->getNomListing());
        }
        //dump($tableProduit);
        //exit();
        //dd($listUsers);

       // exit();

        return $this->render('dashboard/index.html.twig', [
            'controller_name' => "Project courses partagées ",
            'list_users' => $listUsers,
            'list_produits' => $tableProduit,
        ]);
    }
}
