<?php

namespace App\Controller;

use App\Entity\ListUser;
use App\Form\ListUserType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/list/user")
 */
class ListUserController extends AbstractController
{
    /**
     * @Route("/", name="list_user_index", methods={"GET"})
     */
    public function index(): Response
    {
        $listUsers = $this->getDoctrine()
            ->getRepository(ListUser::class)
            ->findAll();

        return $this->render('list_user/index.html.twig', [
            'list_users' => $listUsers,
        ]);
    }

    /**
     * @Route("/new", name="list_user_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $listUser = new ListUser();
        $form = $this->createForm(ListUserType::class, $listUser);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($listUser);
            $entityManager->flush();

            return $this->redirectToRoute('list_user_index');
        }

        return $this->render('list_user/new.html.twig', [
            'list_user' => $listUser,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{idUserList}", name="list_user_show", methods={"GET"})
     */
    public function show(ListUser $listUser): Response
    {
        return $this->render('list_user/show.html.twig', [
            'list_user' => $listUser,
        ]);
    }

    /**
     * @Route("/{idUserList}/edit", name="list_user_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, ListUser $listUser): Response
    {
        $form = $this->createForm(ListUserType::class, $listUser);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('list_user_index');
        }

        return $this->render('list_user/edit.html.twig', [
            'list_user' => $listUser,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{idUserList}", name="list_user_delete", methods={"POST"})
     */
    public function delete(Request $request, ListUser $listUser): Response
    {
        if ($this->isCsrfTokenValid('delete'.$listUser->getIdUserList(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($listUser);
            $entityManager->flush();
        }

        return $this->redirectToRoute('list_user_index');
    }
}
