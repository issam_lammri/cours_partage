<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Listing
 *
 * @ORM\Table(name="listing")
 * @ORM\Entity
 */
class Listing
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_listing", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idListing;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_listing", type="string", length=50, nullable=false)
     */
    private $nomListing;

    /**
     * @var string|null
     *
     * @ORM\Column(name="descreption_listing", type="string", length=255, nullable=true)
     */
    private $descreptionListing;

    /**
     * @var bool
     *
     * @ORM\Column(name="finaly", type="boolean", nullable=false)
     */
    private $finaly;

    /**
     * @var int
     *
     * @ORM\Column(name="price_total", type="integer", nullable=false)
     */
    private $priceTotal;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_creat", type="date", nullable=false)
     */
    private $dateCreat;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="date_fin", type="date", nullable=true)
     */
    private $dateFin;

    public function getIdListing(): ?int
    {
        return $this->idListing;
    }

    public function getNomListing(): ?string
    {
        return $this->nomListing;
    }

    public function setNomListing(string $nomListing): self
    {
        $this->nomListing = $nomListing;

        return $this;
    }

    public function getDescreptionListing(): ?string
    {
        return $this->descreptionListing;
    }

    public function setDescreptionListing(?string $descreptionListing): self
    {
        $this->descreptionListing = $descreptionListing;

        return $this;
    }

    public function getFinaly(): ?bool
    {
        return $this->finaly;
    }

    public function setFinaly(bool $finaly): self
    {
        $this->finaly = $finaly;

        return $this;
    }

    public function getPriceTotal(): ?int
    {
        return $this->priceTotal;
    }

    public function setPriceTotal(int $priceTotal): self
    {
        $this->priceTotal = $priceTotal;

        return $this;
    }

    public function getDateCreat(): ?\DateTimeInterface
    {
        return $this->dateCreat;
    }

    public function getDateCreatToString()
    {
        return  $this->dateCreat->format('Y-m-d ');;
    }

    public function setDateCreat(\DateTimeInterface $dateCreat): self
    {
        $this->dateCreat = $dateCreat;

        return $this;
    }

    public function getDateFin(): ?\DateTimeInterface
    {
        return $this->dateFin;
    }

    public function setDateFin(?\DateTimeInterface $dateFin): self
    {
        $this->dateFin = $dateFin;

        return $this;
    }

    public function __toString()
    {
        return (string)$this->getNomListing();
    }
}
