<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Product
 *
 * @ORM\Table(name="product", indexes={@ORM\Index(name="id_listing", columns={"id_listing"})})
 * @ORM\Entity
 */
class Product
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_product", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idProduct;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=50, nullable=false)
     */
    private $nom;

    /**
     * @var int
     *
     * @ORM\Column(name="prix", type="integer", nullable=false)
     */
    private $prix;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_creat", type="date", nullable=false)
     */
    private $dateCreat;

    /**
     * @var bool
     *
     * @ORM\Column(name="fin", type="boolean", nullable=false)
     */
    private $fin;

    /**
     * @var \Listing
     *
     * @ORM\ManyToOne(targetEntity="Listing")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_listing", referencedColumnName="id_listing")
     * })
     */
    private $idListing;

    public function getIdProduct(): ?int
    {
        return $this->idProduct;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrix(): ?int
    {
        return $this->prix;
    }

    public function setPrix(int $prix): self
    {
        $this->prix = $prix;

        return $this;
    }

    public function getDateCreat(): ?\DateTimeInterface
    {
        return $this->dateCreat;
    }

    public function setDateCreat(\DateTimeInterface $dateCreat): self
    {
        $this->dateCreat = $dateCreat;

        return $this;
    }

    public function getFin(): ?bool
    {
        return $this->fin;
    }
    public function getDateCreatToString()
    {
        return  $this->dateCreat->format('Y-m-d ');;
    }
    public function setFin(bool $fin): self
    {
        $this->fin = $fin;

        return $this;
    }

    public function getIdListing(): ?Listing
    {
        return $this->idListing;
    }

    public function setIdListing(?Listing $idListing): self
    {
        $this->idListing = $idListing;

        return $this;
    }


}
